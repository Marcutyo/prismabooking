package it.prisma.prismabookinggit.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import it.prisma.prismabookinggit.model.PagedResponse;

import it.prisma.prismabookinggit.component.ConfigurationComponent;

public class BaseService<T> {

    protected ConfigurationComponent config;
    protected List<T> list = new ArrayList<>();

    public PagedResponse<T> findPage(Integer index, Integer limit) {
        limit = handleLimit(limit);
        index = Optional.ofNullable(index).orElse(0);
        Integer start = index * limit;

        var pageContent = list.subList(start, start+limit);
        return PagedResponse.<T>builder()
                .data(pageContent)
                .index(index.longValue())
                .totalElements((long) list.size())
                .build();
    }

    public T addResource(T resource) {
        list.add(resource);
        return resource;
    }

    protected Integer handleLimit(Integer limit) {
        limit = Optional.ofNullable(limit)
                .filter(l -> l <= config.getMaxPageLimit())
                .orElse(config.getDefaultPageLimit());

        if(limit > list.size()) {
            limit = list.size();
        }
        return limit;
    }
}

