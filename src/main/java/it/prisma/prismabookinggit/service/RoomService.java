package it.prisma.prismabookinggit.service;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import it.prisma.prismabookinggit.model.room.Room;

@Service
public class RoomService {
	public Iterable<Room> getAll(){
		return null;
	};
	public Optional<Room> getById(@PathVariable("id") int id){
		return null;
	};
	public Room createRoom(Room room){
		return null;
	};
	public Optional<Room> updateRoom(@PathVariable int id, @RequestBody Room room){
		return null;
	};
	public boolean delete(int id){
		return false;
	};

}
