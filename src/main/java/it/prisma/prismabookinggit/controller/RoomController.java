package it.prisma.prismabookinggit.controller;
;
import com.prisma.prismabooking.model.PagedResponse;
import it.prisma.prismabookinggit.service.RoomService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Book;

@RestController
@RequestMapping("/api/v1/facility/{id-facility}/rooms")
public class RoomController {

    //todo add profiles
    final RoomService roomService;

    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @Operation(summary = "Get a page of structure's rooms")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the page",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PagedResponse.class)) }) })
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    //TODO find bug in this API
    public PagedResponse<Room> findStructurePage(@PathVariable("structureId") String structureId,
                                                 @RequestParam(required = false) Integer offset,
                                                 @RequestParam(required = false) Integer limit) {
        return roomService.findPage(structureId, offset, limit);
    }

    @Operation(summary = "Find a room by structureId and roomId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the room",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Room.class)) }),
            @ApiResponse(responseCode = "404", description = "Room not found",content = @Content )})
    @GetMapping("/{id-room}}")
    @ResponseStatus(HttpStatus.OK)
    public Room findStructure(@PathVariable("structureId") String structureId,
                              @PathVariable("roomId") String roomId) {
        return roomService.findRoom(structureId, roomId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Room createStructure(@PathVariable("structureId") String structureId,
                                @RequestBody Room room) {
        if(room.getId() != null)
            throw new BadRequestException("Cannot POST an existing resource");
        return roomService.createRoom(structureId, room);
    }

    @PutMapping("/{id-room}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Room updateStructure(@PathVariable("structureId") String structureId,
                                @PathVariable("roomId") String roomId,
                                @RequestBody Room room) {
        return roomService.createRoom(structureId, room);
    }

    @DeleteMapping("/{id-room}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStructure(@PathVariable("structureId") String structureId,
                                @PathVariable("roomId") String roomId) {
        roomService.deleteFromFile(structureId, roomId);
    }

}
