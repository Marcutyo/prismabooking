package it.prisma.prismabookinggit.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/facilities")
public class StructureController {

    private final StructureService structureService;

    public StructureController(StructureService structureService) {
        this.structureService = structureService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public PagedResponse<Structure> findStructurePage(@RequestParam(required = false) Integer offset,
                                                      @RequestParam(required = false) Integer limit) {
        return structureService.findPage(offset, limit);
    }

    @GetMapping("/{Id}")
    @ResponseStatus(HttpStatus.OK)
    public Structure findStructure(@PathVariable("structureId") String structureId) {
        return null;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Structure createStructure(@RequestBody Structure structure) {
        return structure;
    }

    @PutMapping("/{Id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Structure updateStructure(@PathVariable("structureId") String structureId,
                                     @RequestBody Structure structure) {
        return structure;
    }

    @DeleteMapping("/{Id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStructure(@PathVariable("structureId") String structureId) {

    }

}
