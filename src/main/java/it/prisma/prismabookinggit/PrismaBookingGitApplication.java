package it.prisma.prismabookinggit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrismaBookingGitApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrismaBookingGitApplication.class, args);
    }

}
