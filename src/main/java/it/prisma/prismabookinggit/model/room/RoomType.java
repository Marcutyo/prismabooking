package it.prisma.prismabookinggit.model.room;

public enum RoomType {
    ECONOMY,
    FAMILY,
    JUNIOR_SUITE,
    DELUXE_SUITE
}
