package it.prisma.prismabookinggit.model.user;

public enum UserType {
    ADMINISTRATOR,
    MANAGER,
    RECEPTIONIST,
    CUSTOMER
}
