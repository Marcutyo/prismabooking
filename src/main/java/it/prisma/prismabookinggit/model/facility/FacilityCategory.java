package it.prisma.prismabookinggit.model.facility;

public enum FacilityCategory {
    SPA,
    RESORT,
    HOTEL,
    BNB,
    GUESTHOUSE
}
