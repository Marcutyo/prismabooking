package it.prisma.prismabookinggit.model.facility;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Facility {
    private Long id;
    private String name;
    private String address;
    private String city;
    private String country;
    private String email;
    private Boolean hasWifi;
    private String phoneNumber;
    private FacilityCategory category;
}
